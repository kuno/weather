# 天气预报

#### 介绍
使用百度地图开放平台api获取天气数据，界面参考于UI中国，对于小程序初学者有很大的帮助，不需要模拟调用后台接口，可以很好的入门学习

#### 安装教程

1. 下载微信web开发者工具[下载地址](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)  

2. 导入目录文件

#### 使用说明

1.小程序已认证,有appID,导入项目时修改appId

2.必须把https://api.map.baidu.com 添加到小程序的合法域名列表中
  ![小程序的合法域名列表](https://images.gitee.com/uploads/images/2019/0716/110403_31557c6c_5122567.png "屏幕截图.png")

3.在百度开放平台上创建一个应用，拿到Ak(前提有百度账号)
  申请地址：http://lbsyun.baidu.com/apiconsole/key
  ![输入图片说明](https://images.gitee.com/uploads/images/2019/0716/101635_ffaa7da8_5122567.png "屏幕截图.png")
  ![输入图片说明](https://images.gitee.com/uploads/images/2019/0716/101648_3398f7fe_5122567.png "屏幕截图.png")

4.修改pages/main/main.js 中loadCity方法的Url,将ak的值改为自己对应百度应用的ak值
   `wx.request({
        url: 'http://api.map.baidu.com/geocoder/v2/?ak= **修改此处** `

#### 界面效果

![雨夹雪](https://images.gitee.com/uploads/images/2019/0716/111606_5e655245_5122567.jpeg "1313.jpg")
![雪](https://images.gitee.com/uploads/images/2019/0716/122011_7aa7a485_5122567.jpeg "543636.jpg")
![冰雹](https://images.gitee.com/uploads/images/2019/0716/122028_c8fffc1c_5122567.jpeg "134.jpg")
![雾霾](https://images.gitee.com/uploads/images/2019/0716/132703_965cfdcc_5122567.jpeg "342.jpg")
![沙尘暴](https://images.gitee.com/uploads/images/2019/0716/132719_314918a5_5122567.jpeg "未标题-3.jpg")
![总图](https://images.gitee.com/uploads/images/2019/0716/133046_a86f1900_5122567.jpeg "36.jpg")
